<?php

namespace App\Http\Controllers;

use App\Paypal\SubscriptionPlan;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     *
     */
    public function createPlan()
    {
        $plan = new SubscriptionPlan();
        $plan->create();
    }
    /**
     * @return \PayPal\Api\PlanList
     */
    public  function listPlan()
    {
        $plan = new SubscriptionPlan();
        return $plan->listPlan();
    }

    public function showPlan($id)
    {
        $plan = new SubscriptionPlan();
        return $plan->planDetails($id);
    }

    public function activatePlan($id)
    {
        $plan = new SubscriptionPlan();
        $plan->activate($id);
    }
}
